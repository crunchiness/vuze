/*
 * File    : CountryItem.java
 * Created : November 16, 2012
 * By      : Ingvaras Merkys
 *
 * Copyright (C) 2004, 2005, 2006 Aelitis SAS, All rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details ( see the LICENSE file ).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * AELITIS, SAS au capital de 46,603.30 euros,
 * 8 Allee Lenotre, La Grille Royale, 78600 Le Mesnil le Roi, France.
 */
 
package org.gudy.azureus2.ui.swt.views.tableitems.peers;

import java.io.File;
import java.io.IOException;

import org.eclipse.swt.graphics.Image;
import org.gudy.azureus2.core3.internat.MessageText;
import org.gudy.azureus2.core3.peer.PEPeer;
import org.gudy.azureus2.core3.util.AERunnable;
import org.gudy.azureus2.ui.swt.Utils;
import org.gudy.azureus2.ui.swt.debug.ObfusticateCellText;
import org.gudy.azureus2.ui.swt.pluginsimpl.UISWTGraphicImpl;
import org.gudy.azureus2.ui.swt.views.table.utils.CoreTableColumn;

import org.gudy.azureus2.plugins.ui.tables.TableCell;
import org.gudy.azureus2.plugins.ui.tables.TableCellRefreshListener;
import org.gudy.azureus2.plugins.ui.tables.TableColumnInfo;

import com.aelitis.azureus.geoip.*;
import com.aelitis.azureus.ui.swt.imageloader.ImageLoader;

/**
 *
 * @author Ingvaras Merkys
 * @created November 19, 2012
 * 
 */
public class CountryItem
       extends CoreTableColumn 
       implements TableCellRefreshListener, ObfusticateCellText
{
	static final int COLUMN_WIDTH = 40;
	public static final String COLUMN_ID = "country";
	private static final String dbfile = new File(".").getAbsolutePath() + System.getProperty("file.separator") + "GeoIP.dat";
	private LookupService cl;

	public void fillTableColumnInfo(TableColumnInfo info) {
		info.addCategories(new String[] {
			CAT_PEER_IDENTIFICATION,
			CAT_CONNECTION
		});
	}

  /** Default Constructor */
  public CountryItem(String table_id) {
    super(COLUMN_ID, POSITION_LAST, COLUMN_WIDTH, table_id);
    initializeAsGraphic(POSITION_LAST, COLUMN_WIDTH);
	setMinWidth(COLUMN_WIDTH);
    setObfustication(true);
  		  try {
  				this.cl = new LookupService(dbfile,LookupService.GEOIP_MEMORY_CACHE);
  			} catch (IOException e1) {
  				System.out.println("GeoIP.dat not found at " + dbfile);
  				this.cl = null;
  			};
   }

  public void refresh(final TableCell cell) {
    PEPeer peer = (PEPeer)cell.getDataSource();
    String ip = (peer == null) ? "" : peer.getIp();
    final String country_code = (cl == null) ? "N_A" : cl.getCountry(ip).getCode();
    final String flag_name = (cl == null) ? "flag_unknown" : cl.getCountry(ip).getFlag();
    
    Utils.execSWTThread(new AERunnable() {
		public void runSupport() {
			ImageLoader imageLoader = ImageLoader.getInstance();
			Image img = imageLoader.getImage(flag_name);
			try {
				if (img != null && !img.isDisposed()) {
	  			cell.setGraphic(new UISWTGraphicImpl(img));
				}
			} finally {
				imageLoader.releaseImage(flag_name);
			}
		}
	});
	
	String country_name = MessageText.getString("country." + country_code.toLowerCase());
	cell.setSortValue(country_name);
    cell.setToolTip(country_name);
  }
  public String getObfusticatedText(TableCell cell) {
	  	return (String) cell.getSortValue();
	  }
}
