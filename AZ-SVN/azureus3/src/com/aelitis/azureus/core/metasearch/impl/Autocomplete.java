/**
 * @author Ingvaras Merkys
 * @created 19 November, 2012
 * 
 */
package com.aelitis.azureus.core.metasearch.impl;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class Autocomplete {

	public static <K,V extends Comparable<? super V>>
	SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
	    SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
	    		new Comparator<Map.Entry<K,V>>() {
	            @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	                int comparison = -e1.getValue().compareTo(e2.getValue());
	                if (comparison == 0) return -1; else return comparison;
	            }
	        }
	    );
	    sortedEntries.addAll(map.entrySet());
	    return sortedEntries;
	}
	
	public static String[] autocomplete(char character) {
		return Autocomplete.autocomplete(character + "");
	}

	public static String[] autocomplete(String string) {
		string = string.toLowerCase();
		String[] suggestions = {"", "", "", "", ""};
		Map<String,Long> searchHistory = (new SearchHistory()).getSearchHistory();
		SortedSet<java.util.Map.Entry<String, Long>> sortedHistory = entriesSortedByValues(searchHistory);
    	Iterator<java.util.Map.Entry<String, Long>> iterator = sortedHistory.iterator();
		
    	int count = 0;
    	while (iterator.hasNext() && count<5) {
    		java.util.Map.Entry<String, Long> element = iterator.next();
    		String key = element.getKey();
    		if (key.startsWith(string)) {
    			suggestions[count] = key;
    			count++;
			}
    	}
    	return suggestions;
	}
/*//test
	public static void main(String[] args) {
		String[] something = autocomplete("s");
		for (String s : something) {
			System.out.println("s: " + s);
		}
	}
*/
}