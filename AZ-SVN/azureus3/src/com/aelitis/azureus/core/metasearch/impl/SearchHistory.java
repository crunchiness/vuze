/**
 * @author Ingvaras Merkys
 * @created 19 November, 2012
 * 
 */
package com.aelitis.azureus.core.metasearch.impl;

import java.io.File;
import java.util.Map;

import org.gudy.azureus2.core3.util.FileUtil;
import org.gudy.azureus2.core3.util.SystemProperties;

public class SearchHistory {
private static final String file_name = "searchhistory.config";
private static Map<String, Long> searchHistory;
private static File parent_dir;
    static {
    	parent_dir = new File(SystemProperties.getUserPath());
//	  	boolean use_backups = COConfigurationManager.getBooleanParameter("Use Config File Backups" ); 
		searchHistory = FileUtil.readResilientFile( parent_dir, file_name, false );
    }
    
	public static void add(String query) {
		String queryl = query.toLowerCase();
		if (searchHistory.containsKey(queryl)) {
			int searchInstances = searchHistory.get(queryl).intValue();
			searchInstances++;
			searchHistory.put(queryl, new Long(searchInstances));
		} else {
			searchHistory.put(queryl, new Long(1));
		}
		FileUtil.writeResilientFile(parent_dir, file_name, searchHistory, false);
		System.out.println(parent_dir + ", " + file_name +", " + searchHistory.toString());
	}
	public Map getSearchHistory() {
		return searchHistory;
	}
}
